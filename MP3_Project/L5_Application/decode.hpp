/*
 * decode.hpp
 *
 *  Created on: Apr 30, 2018
 *      Author: Nicholas Kaiser
 */

#ifndef DECODE_HPP_
#define DECODE_HPP_

//#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "utilities.h"
#include "lpc_sys.h"
#include "tasks.hpp"
#include "ssp0.h"
#include "gpio.hpp"
#include <string.h>
#include "MP3_Tasks.hpp"
#include "UART_LCD.hpp"

typedef enum {
    WRITE = 0x02,
    READ = 0x03
}rd_wr;

typedef enum {
    SCI_MODE_ADDR = 0x00,
    SCI_STATUS_ADDR = 0x01,
    SCI_BASS_ADDR = 0x02,
    SCI_CLOCKF_ADDR = 0x03,
    SCI_DECODE_TIME_ADDR = 0x04,
    SCI_AUDATA_ADDR = 0x05,
    SCI_WRAM_ADDR = 0x06,
    SCI_WRAMADDR_ADDR = 0x07,
    SCI_HDAT0_ADDR = 0x08,
    SCI_HDAT1_ADDR = 0x09,
    SCI_AIADDR_ADDR = 0x0A,
    SCI_VOL_ADDR = 0x0B,
    SCI_AICTRL0_ADDR = 0x0C,
    SCI_AICTRL1_ADDR = 0x0D,
    SCI_AICTRL2_ADDR = 0x0E,
    SCI_AICTRL3_ADDR = 0x0F
}SCI_REG;

//initializes SSP0 and decoder in order to play an MP3 file
void init(uint8_t vol);

//initializes SSP0 and decoder to perform a sine test
void sine_test_init(uint8_t vol);

//writes 32 bytes of data (MP3 file) over the serial data interface
uint8_t* SDI_write(uint8_t *buffer);//originally uint8_t

//writes data over the serial control interface to configure SCI registers
void SCI_write(SCI_REG reg, uint8_t MSBdata, uint8_t LSBdata);

//reads data over the serial control interface to obtain the current value of an SCI register
uint16_t SCI_read(SCI_REG reg);

//performs a sine test at a user specified volume level and frequency
void sine_test(uint8_t frequency, uint8_t vol);

//plays SD card MP3 data
void varinder_play_MP3();

//performs a hardware reset
void hardware_reset();

//performs a software reset and then restores original volume level
void software_reset();//in progress

//enables the serial control interface's chip select
void enable_xcs();

//disables the serial control interface's chip select
void disable_xcs();

//enables the serial data interface's chip select
void enable_xdcs();

//disables the serial data interface's chip select
void disable_xdcs();

//returns logic level of DREQ pin
uint8_t get_DREQ();

#endif /* DECODE_HPP_ */
