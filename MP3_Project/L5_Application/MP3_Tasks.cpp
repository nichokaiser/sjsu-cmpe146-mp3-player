#include <stdio.h>
#include <stdint.h>
#include "utilities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "uart0_min.h"
#include "printf_lib.h"
#include "LPC17xx.h"
#include "semphr.h"
#include "lpc_isr.h"
#include "io.hpp"
#include <time.h>
#include <storage.hpp>
#include "event_groups.h"
#include "command_handler.hpp"
#include "lpc_sys.h"
#include "tasks.hpp"
#include "ssp1.h"
#include "ssp0.h"
#include "gpio.hpp"
#include "decode.hpp"
#include "MP3_Tasks.hpp"
#include "UART_LCD.hpp"


#define LINE_LENGTH 40
#define MAX_SONGS 5


//Bass register configurations
//ST_FREQLIMIT: 5000Hz (bits 8-11)
//SB_FREQLIMIT: 60Hz (bits 0-3)
//VSBE: Bass enhancement
//VSTC: Treble enhancement

static uint8_t volume = 0x3F;//Default: 0x6F
static uint8_t vsbe = 0x00;//write this to the SCI_BASS register's least significant 8-bits
static uint8_t vstc = 0x00;//write this to the SCI_BASS register's most significant 8-bits
int numb_of_tracks = 0;
int current_track = 0;
int phrase_length = 50;

//play pause switching variables
bool first = true;
bool play = false;
bool pause = false;
bool stop = false;

uint8_t get_val_vol() {
    volume = SCI_read(SCI_VOL_ADDR);
    return volume;
}

uint8_t get_val_vsbe() {//LSB of BASS_ADDR
    vsbe = (SCI_read(SCI_BASS_ADDR));
    return vsbe;
}

uint8_t get_val_vstc() {//MSB of BASS_ADDR
    vstc = (SCI_read(SCI_BASS_ADDR) >> 8);
    return vstc;
}

int get_numb_of_tracks() {
    return numb_of_tracks;
}

void set_val_vol(uint8_t vol) {
    SCI_write(SCI_VOL_ADDR, vol, vol);
}

void set_val_eq(uint8_t treb, uint8_t bass) {
    SCI_write(SCI_BASS_ADDR, treb, bass);
}

void set_numb_of_tracks(int val) {
    numb_of_tracks = val;
}

void incr_vol() {
    volume--; //decrease register value to increase output volume
    SCI_write(SCI_VOL_ADDR, volume, volume);
}

void decr_vol() {
    volume++; //increase register value to increase output volume
    SCI_write(SCI_VOL_ADDR, volume, volume);
}

void volEncoder(void *p)
{
    uint8_t volume_buffer = volume;
    uint8_t clkVal = 1;
    uint8_t datVal = 1;
    uint8_t mute = 0;
    uint8_t prev_state = 0;
    uint8_t toggle = 0;
    uint8_t lastState = clkVal;


    //SW P2.5
    LPC_GPIO2->FIODIR &= ~(1 << 5);
    LPC_PINCON->PINMODE4 |= (3 << 10);
    LPC_PINCON->PINSEL4 &= ~(3 << 10);
    //DAT P2.6
    LPC_GPIO2->FIODIR &= ~(1 << 6);
    LPC_PINCON->PINMODE4 |= (3 << 12);
    LPC_PINCON->PINSEL4 &= ~(3 << 12);
    //CLK P2.7
    LPC_GPIO2->FIODIR &= ~(1 << 7);
    LPC_PINCON->PINMODE4 |= (3 << 14);
    LPC_PINCON->PINSEL4 &= ~(3 << 14);

    while (1) {
        //MUTE
        if(LPC_GPIO2->FIOPIN & (1 << 5)){
            toggle = 1;
        }
        else{
            toggle = 0;
        }
        if(toggle == 0 && prev_state == 1){
            if(!mute){
                mute = 1;
            }
            else{
                mute = 0;
            }
        }

        //VOLUME
        lastState = datVal;
        if(LPC_GPIO2->FIOPIN & (1 << 6)){
            clkVal = 1;
        }
        else{
            clkVal = 0;
        }

        if(LPC_GPIO2->FIOPIN & (1 << 7)){
            datVal = 1;
        }
        else{
            datVal = 0;
        }

        //LOGIC
        if(mute){
            if(volume < 0xFE) {
                volume_buffer = volume;
            }
            volume = 0xFE;
        }
        else{
            volume = volume_buffer;
            if(datVal != lastState){
                    if(datVal != clkVal){
                        if (volume < 0xFE) volume++; //incr_vol();
                    }
                    else{
                        if (volume > 0x00) volume--; //decr_vol();
                    }
                }
            volume_buffer = volume;
        }

        prev_state = toggle;
        vTaskDelay(2); //delay poll and send by 2 RTOS ticks(2 millisecond)
    }
}

void eqEncoder(void *p)
{
    uint8_t sb_amp = 0x00; //bits [7:4] of SCI_BASS, controls bass amplitude
    uint8_t st_amp = 0x00; //bits [15:12] of SCI_BASS, controls treble amplitude
    uint8_t sb_freq = 0x06; //bits [3:0] of SCI_BASS, controls bass frequency cutoff (60Hz by default)
    uint8_t st_freq = 0x05; //bits [11:8] of SCI_BASS, controls treble frequency cutoff (5kHz by default)

    vsbe = sb_freq; //write this to the SCI_BASS register's least significant 8-bits
    vstc = st_freq; //write this to the SCI_BASS register's most significant 8-bits

    uint8_t clkVal = 1;
    uint8_t datVal = 1;
    uint8_t eqBass = 0; //this is 0 for bass control, 1 for treble control
    uint8_t prev_state = 0;
    uint8_t toggle = 0;
    uint8_t lastState = clkVal;

    //SW P0.0
    LPC_GPIO0->FIODIR &= ~(1 << 0);
    LPC_PINCON->PINMODE0 |= (3 << 0);
    LPC_PINCON->PINSEL0 &= ~(3 << 0);
    //DAT P0.1
    LPC_GPIO0->FIODIR &= ~(1 << 1);
    LPC_PINCON->PINMODE0 |= (3 << 2);
    LPC_PINCON->PINSEL0 &= ~(3 << 2);
    //CLK P0.30
    LPC_GPIO0->FIODIR &= ~(1 << 30);
    LPC_PINCON->PINMODE1 |= (3 << 28);
    LPC_PINCON->PINSEL1 &= ~(3 << 28);

    while (1) {
        //Toggle between bass and treble adjustments
        if(LPC_GPIO0->FIOPIN & (1 << 0)){
            toggle = 1;
        }
        else{
            toggle = 0;
        }
        if(toggle == 0 && prev_state == 1){
            if(!eqBass){
                eqBass = 1;
            }
            else{
                eqBass = 0;
            }
        }

        //Read from Encoder
        lastState = datVal;
        if(LPC_GPIO0->FIOPIN & (1 << 30)){
            clkVal = 1;
        }
        else{
            clkVal = 0;
        }

        if(LPC_GPIO0->FIOPIN & (1 << 1)){
            datVal = 1;
        }
        else{
            datVal = 0;
        }

        //LOGIC
        if(datVal != lastState){
            if(datVal != clkVal){
                if(eqBass){
                    if (st_amp < 15){
                        st_amp++;
                    }
                    else{
                        st_amp = 15;
                    }
                }
                else{
                    if (sb_amp < 15){
                        sb_amp++;
                    }
                    else{
                        sb_amp = 15;
                    }
                }

            }
            else{
                if(eqBass){
                    if (st_amp > 0){
                        st_amp--;
                    }
                    else{
                        st_amp = 0;
                    }
                }
                else{
                    if (sb_amp > 0){
                        sb_amp--;
                    }
                    else{
                        sb_amp = 0;
                    }
                }
            }
        }
        prev_state = toggle;

        vstc &= ~(0xF << 4);
        vstc |= (st_amp << 4);

        vsbe &= ~(0xF << 4);
        vsbe |= (sb_amp << 4);
        //printf("bass = %x treble = %x\n", get_val_vstc(), get_val_vsbe());
        vTaskDelay(2); //delay poll and send by 2 RTOS ticks(2 millisecond)
    }
}

void suspend(void *p) {
    TaskHandle_t playSoundHandle = (TaskHandle_t)p;

    while (1) {
        if (first) {
            vTaskSuspend(playSoundHandle);
        }
        vTaskDelay(100);
    }
}


void lcdDisplay(void *p){
    char str[LINE_LENGTH];
    memset(str, ' ', LINE_LENGTH);

/*
    for(int w=0;w<sizeofsong;w++){
        strcpy((char*)str[w],pntr[current_track].song_name);
    }
*/

    mp3_t* pntr= (mp3_t*) p;
    //int sizeofsong= 0; //sizeof(pntr[current_track].song_name);
    //int incr= 0;


//   while(1){
//        if(pntr[current_track].song_name[incr]=='.'){
//            break;
//        }else{
//            str[incr]=pntr[current_track].song_name[incr];
//        }
//        incr++;
//    }

    LCD_UART obj;
    obj.init2();
    delay_ms(100);

    //BASIC SETUP
    //turn display off
    obj.transfer2(0x15);
    delay_ms(100);
    //turn display on  without cursor
    obj.transfer2(0x16);
    delay_ms(100);
    //turn backlight on
    obj.transfer2(0x11);
    delay_ms(100);
    //clear display and reset cursor to line 0 position 0
    obj.transfer2(0x0C);
    delay_ms(100);


    while(1){
        memset(str, ' ', LINE_LENGTH);
        for(int j=0;j<sizeof(pntr[current_track].song_name);j++){ //sizeof(pntr[current_track].song_name)
            if(pntr[current_track].song_name[j]=='.' && pntr[current_track].song_name[j+1]=='m'){
                break;
            }else{
                str[j]=pntr[current_track].song_name[j];
            }
        }


        for(int i=0;i<LINE_LENGTH;i++){
            //if(pntr[current_track].song_name[i]!=NULL){
            if(str[i]!=NULL){

                //obj.transfer2((uint8_t)pntr[current_track].song_name[i]);
                obj.transfer2((uint8_t)str[i]);
            }else{
                obj.transfer2(' ');
            }

            //if(i==19) obj.transfer2(0X80);
            delay_ms(50);
        }

        //SCI_READ(SCI_DECODE_TIME_ADDR);

        obj.transfer2(0X80);
        vTaskDelay(100); //500

    }

}


void playSound(void *p) {
    mp3_t* mp3 = (mp3_t*)p;

    //Setup buffer, SD card, and init decoder
    uint8_t buffer[512] = {0};
    uint8_t *mp3_ptr = buffer;
    uint8_t *buff_ptr = buffer;
    int track_playing = 0;
    init(volume);

    FATFS FatFs;
    FIL mp3_file;
    FRESULT ret_val;

    UINT buff_size = sizeof(buffer);
    unsigned int bytes_rd = 0;

    f_mount(&FatFs, "", 0);

    while (1) {
        ret_val = f_open(&mp3_file, mp3[current_track].directory_name, FA_READ);
        //Send two zeros before we send MP3 data
        SDI_write(buff_ptr); //0
        SDI_write(buff_ptr); //0
        track_playing = current_track;
        while (!f_eof(&mp3_file)) {
            f_read(&mp3_file, mp3_ptr, buff_size, &bytes_rd);
            for(unsigned int i=0; i<sizeof(buffer); i=i+32) {
                //MUST DO! When its low wait for a rising edge interrupt and if its low
                //Check data request pin is low, loop until its high
                while(!get_DREQ()){
                    vTaskDelay(1); //reduce CPU Usage
                }
                buff_ptr = SDI_write(buff_ptr);
            }
            if(track_playing != current_track){//DEF NEED
                break;
            }//comment out? causing issues with next and previous
            buff_ptr = buffer;
            set_val_vol(volume);
            set_val_eq(vstc, vsbe);
            vTaskDelay(0);
        }

        memset(buffer, 0, sizeof(buffer)); //clears buffer
        buff_ptr = buffer;
        for(int i = 0; i < sizeof(buffer); i++){
            SDI_write(buff_ptr); //0
        }
        delay_ms(50);
        f_close(&mp3_file);

        if(track_playing == current_track){
            current_track++;
        }//comment out? causing issues with next and previous
        if(current_track == get_numb_of_tracks()){
            current_track = 0;
        }
    }//end while loop
}//end task

int next_song() {
    if (current_track == get_numb_of_tracks()) {
        current_track = 0;
        printf("GPIO P2.3: current_track = %d\n", current_track);
    }
    else {
        current_track++;
        printf("GPIO P2.3: current_track = %d\n", current_track);
    }
    return current_track;
}

int prev_song() {
    if (get_numb_of_tracks() == 2) {
        if (current_track == 1) {
            current_track = 0;
        }
        else {
            current_track = 0;
        }
    }
    else {
        if (current_track == 0) {
            current_track = 0;//get_numb_of_tracks(); or 0
            printf("GPIO P2.0: current_track = %d\n", current_track);
        }
        else {
            current_track--;
            printf("GPIO P2.0: current_track = %d\n", current_track);
        }
    }
    return current_track;
}

void play_pause(TaskHandle_t gpioPanelHandle, TaskHandle_t eqEncoderHandle, TaskHandle_t volEncoderHandle, TaskHandle_t playSoundHandle, TaskHandle_t suspendHandle) {
    if (first) {
        vTaskSuspend(suspendHandle);
        first = false;
        play = true;
        pause = false;
        stop = false;
        vTaskResume(playSoundHandle);
    }
    else {
        if (stop) {
            play = true;
            pause = false;
            stop = false;
            vTaskResume(playSoundHandle);
        }
        else {
            if (pause) {
                pause = false;
                play = true;
                stop = false;
                vTaskResume(playSoundHandle);
            }
            else {
                play = false;
                pause = true;
                stop = false;
                vTaskSuspend(playSoundHandle);
            }
        }//end else 2
    }//end else 1
}//end function

int stop_reset(TaskHandle_t gpioPanelHandle, TaskHandle_t eqEncoderHandle, TaskHandle_t volEncoderHandle, TaskHandle_t playSoundHandle, TaskHandle_t suspendHandle) {
    vTaskSuspend(playSoundHandle);
    current_track = 0;
    stop = true;
    play = false;
    pause = true;
    return current_track;
}

FRESULT numb_of_files(char* path) {//path: Start node to be scanned (***also used as work area***)
    FRESULT res;
    DIR dir;
    static FILINFO fno;

    int mp3_file_count = 0;

    res = f_opendir(&dir, path);/* Open the directory */
    if (res == FR_OK) {
        for (;;) {
            res = f_readdir(&dir, &fno);/* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0) break;/* Break on error or end of dir */
            mp3_file_count++;
        }
        f_closedir(&dir);
    }
    printf("file count: %i\n", mp3_file_count);
    set_numb_of_tracks(mp3_file_count);
    return res;
}

mp3_t* get_file_info() {
    //find number of mp3 files in Music folder
    set_numb_of_tracks(0);
    numb_of_files("1:Music");//input is directory of MP3 files

    mp3_t mp3_file[get_numb_of_tracks()];
    printf("\n\nnumber_of_tracks: %i\n\n", get_numb_of_tracks());
    mp3_t *struct_ptr = mp3_file;

    FRESULT res;
    DIR dir;
    static FILINFO fno;

    char path[50];
    strcpy(path, "1:Music");

    res = f_opendir(&dir, path);                       /* Open the directory */
    if (res == FR_OK) {
        for (int i=0; i<get_numb_of_tracks(); i++) {
            fno.lfname = mp3_file[i].song_name;//fno.lfname is a pointer
            fno.lfsize = 100;//size of song_name array
            //copies lfname into mp3_file[i].song_name
            res = f_readdir(&dir, &fno);                   /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
            //strcpy(mp3_file[i].song_name, fno.fname);//only used if we want fname instead of lfname
            printf("song name %i: %s\n", i, mp3_file[i].song_name);
            strcpy(mp3_file[i].directory_name, path);
            strcat(mp3_file[i].directory_name, "/");//append /
            strcat(mp3_file[i].directory_name, mp3_file[i].song_name);//append song_name[i]
            printf("directory name %i: %s\n", i, mp3_file[i].directory_name);
        }
        f_closedir(&dir);
    }

    return struct_ptr;
}
