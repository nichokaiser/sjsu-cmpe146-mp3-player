/*
 * UART_LCD.hpp
 *
 *  Created on: May 19, 2018
 *      Author: Nicholas Kaiser
 */

#ifndef UART_LCD_HPP_
#define UART_LCD_HPP_

#include "LPC17xx.h"
#include "lpc_isr.h"

class LCD_UART {
    private:

    public:
        LCD_UART();
        ~LCD_UART();
        void init2();
        void transfer2(uint8_t data);
        uint8_t receive2(void);
};

void LCD_test();
void Print_title(char* title, int size);

#endif /* UART_LCD_HPP_ */
