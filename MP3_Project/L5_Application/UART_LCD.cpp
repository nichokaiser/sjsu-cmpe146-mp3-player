/*
 * UART_LCD.cpp
 *
 *  Created on: May 19, 2018
 *      Author: Nicholas Kaiser
 */

#include "LPC17xx.h"
#include "lpc_isr.h"
#include "printf_lib.h"
#include <stdio.h>
#include "lpc_isr.h"
#include "UART_LCD.hpp"
#include <stdlib.h>
#include <stdint.h>
#include "utilities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "string.h"

#define LINE_LENGTH 40
#define MAX_SONGS 5


LCD_UART::LCD_UART() {

}

LCD_UART::~LCD_UART() {

}

void LCD_UART::init2() {
      // 1. Power PCUART2 pin 24
      LPC_SC->PCONP |=(1<<24);

      // 2. Peripherial Clock: PCLOCKSEL1 17:16
      LPC_SC->PCLKSEL1&= ~(3<<16);
      //96MHz at the moment
      LPC_SC->PCLKSEL1 |= (1<<16);

      // 3. Baude Rate: we want 9600 BPS, 625= (DLM+DLL)
      // Set DLAB = 1 - enables us to talk with DLL and DLM
      LPC_UART2->LCR |= (1<<7);
      //Set DLL = 113 (0x71)
      LPC_UART2->DLL = 0x71;
      //Set DLM = 2
      LPC_UART2->DLM = 0x02;

      //THEN 8 bit data, 1 stop bit, no parity
      LPC_UART2->LCR |= (3 << 0); //8 bit data length
      LPC_UART2->LCR |= (0 << 2); // Stopbit is set to one
      LPC_UART2->LCR &= ~(1 << 3);//

      // 4. UART FIFO - Enabled FIFO, reset TX* RX buffers*
      LPC_UART2->FCR |= (1 << 0);
      LPC_UART2->FCR |= (1 << 1);
      LPC_UART2->FCR |= (1 << 2);

      // 5. Pins select UART pins through the PINSEL registers
      //TxD2 2[8]
      LPC_PINCON->PINSEL4 &= ~(3 << 16);
      LPC_PINCON->PINSEL4 |= (2 << 16);
      LPC_PINCON->PINMODE4 |= (2 << 16);
      //RxD2 2[9]
      LPC_PINCON->PINSEL4 &= ~(3 << 18);
      LPC_PINCON->PINSEL4 |= (2 << 18);
      LPC_PINCON->PINMODE4 |= (2 << 18);

      // 6. Interrupts: DLAB =0
      LPC_UART2->LCR &= ~(1<<7);
      //U2IER

      //Init UART Rx interrupt (TX interrupt is optional)
      LPC_UART2->IER |= (1 << 2);

      //isr_register(Uart2, my_uart2_rx_intr);
}

void LCD_UART::transfer2(uint8_t data) {
    while (!((LPC_UART2->LSR >> 5) & 0x01)) {
        //wait until THR is empty
    }
    LPC_UART2->THR |= (data << 0);//can use only bits 0-7 for data
}

uint8_t LCD_UART::receive2(void) {
    while (!(LPC_UART2->LSR & 0x01)) {//LSR & (1 < 1)
        //wait until receive FIFO isn't empty
    }
    uint8_t data = LPC_UART2->RBR & 0x0FF;//get only the values of bits 0-7
    return data;
}

void Print_title(char* title, int size){

    unsigned char str[LINE_LENGTH];
    memset(str, ' ', LINE_LENGTH);

    for(int w=0;w<size;w++){
        str[w] = title[w];
    }

    LCD_UART obj;
    obj.init2();
    delay_ms(100);

    //BASIC SETUP
    //turn display off
    obj.transfer2(0x15);
    delay_ms(100);
    //turn display on  without cursor
    obj.transfer2(0x16);
    delay_ms(100);
    //turn backlight on
    obj.transfer2(0x11);
    delay_ms(100);
    //clear display and reset cursor to line 0 position 0
    obj.transfer2(0x0C);
    delay_ms(100);

    while(1){

        for(int i=0;i<LINE_LENGTH;i++){
            obj.transfer2(str[i]);
            if(i==19) obj.transfer2(0X80);
            delay_ms(100);
        }
        obj.transfer2(0X80);
        delay_ms(100);

/*        for(int i=0;i<20;i++){
            obj.transfer2(str[i]);
            delay_ms(100);
            if(i==19){
                obj.transfer2(0X80);
            }
            if(str[i] == NULL){
                i=-1;
                obj.transfer2(0X80);
            }
        }*/
    }

}

void LCD_test() {
    //Parallax LCD Display Baud Rate: 9600bps
    LCD_UART obj;
    obj.init2();
    delay_ms(1000);

    //BASIC SETUP
    //turn display off
    obj.transfer2(0x15);
    delay_ms(100);
    //turn display on  without cursor
    obj.transfer2(0x16);
    delay_ms(100);
    //turn backlight on
    obj.transfer2(0x11);
    delay_ms(100);
    //clear display and reset cursor to line 0 position 0
    obj.transfer2(0x0C);
    delay_ms(100);


    //DISPLAY DATA
    //line 0
    obj.transfer2(0x53);//S
    obj.transfer2(0x6F);//o
    obj.transfer2(0x6E);//n
    obj.transfer2(0x67);//g
    obj.transfer2(0x20);//" "
    obj.transfer2(0x4E);//N
    obj.transfer2(0x61);//a
    obj.transfer2(0x6D);//m
    obj.transfer2(0x65);//e
    obj.transfer2(0x2D);//-
    obj.transfer2(0x41);//A
    obj.transfer2(0x72);//r
    obj.transfer2(0x74);//t
    obj.transfer2(0x69);//i
    obj.transfer2(0x73);//s
    obj.transfer2(0x74);//t

    //line 1
    obj.transfer2(0x30);//0
    obj.transfer2(0x30);//0
    obj.transfer2(0x3A);//:
    obj.transfer2(0x30);//0
    obj.transfer2(0x30);//0
    obj.transfer2(0x20);//" "
    obj.transfer2(0x20);//" "
    obj.transfer2(0x20);//" "
    obj.transfer2(0x20);//" "
    obj.transfer2(0x20);//" "
    obj.transfer2(0x2D);//-
    obj.transfer2(0x30);//0
    obj.transfer2(0x30);//0
    obj.transfer2(0x3A);//:
    obj.transfer2(0x30);//0
    obj.transfer2(0x30);//0
}

