/*
 * decode.cpp
 *
 *  Created on: Apr 30, 2018
 *      Author: Nicholas Kaiser
 */
#include <stdio.h>
#include "utilities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "LPC17xx.h"
#include "semphr.h"
#include "lpc_isr.h"
#include "io.hpp"
#include <time.h>
#include <storage.hpp>
#include "event_groups.h"
#include "command_handler.hpp"
#include "lpc_sys.h"
#include "tasks.hpp"
#include "ssp1.h"
#include "ssp0.h"
#include "gpio.hpp"
#include "decode.hpp"
#include <string.h>
#include "MP3_Tasks.hpp"
#include "UART_LCD.hpp"

//Pin Connections: using ssp0 (preet's driver)
//
//xDCS - P0.1 - orange
//xCS - P0.0 - yellow
//xRESET - P1.19 - purple
//DREQ - P0.30 - blue
//MISO - MISO0 (P0.17) - red
//MOSI - MOSI0 (P0.18) - brown
//SCLK - SCK0 (P0.15) - black

//Op-Code
//
//Read data from decoder: 0x03
//Write data to decoder: 0x02

//Khalil's Logic Analyzer Advice:
//sample (Salae sample speed) at least twice as fast as your clock rate (microcontroller clock speed)

//OLD
//GPIO xCS(P0_0);
//GPIO xDCS(P0_1);
//GPIO DREQ(P0_30);
//GPIO xRESET(P1_19);

//NEW
GPIO xCS(P1_20);
GPIO xDCS(P1_22);
GPIO DREQ(P1_23);
GPIO xRESET(P1_28);

void enable_xcs() {//P0.0
    //set P0.0 to 0
    xCS.setLow();
}

void disable_xcs() {//P0.0
    //set P0.0 to 1
    xCS.setHigh();
}

void enable_xdcs() {//P0.1
    //set P0.1 to 1
    xDCS.setLow();
}

void disable_xdcs() {//P0.1
    //set P0.1 to 0
    xDCS.setHigh();
}

void hardware_reset() {
    xRESET.setLow();
    delay_ms(10);
    xRESET.setHigh();
    delay_ms(10);
}

uint8_t get_DREQ() {
    uint8_t val = DREQ.read();
    return val;
}

void software_reset() {
    //uint16_t vol = 0;

    //vol = SCI_read(SCI_VOL_ADDR);//preserve current volume level

    SCI_write(SCI_VOL_ADDR, 0xFE, 0xFE);//turn sound off
    SCI_write(SCI_MODE_ADDR, 0x08, 0x44);//enable software reset bit, stream, sdinew
    delay_ms(10);
}

void init(uint8_t vol) {
    disable_xdcs();
    disable_xcs();

    xCS.setAsOutput();
    xDCS.setAsOutput();
    DREQ.setAsInput();
    xRESET.setAsOutput();

    //initialize SSP0
    ssp0_init(0);

    //toggle hardware reset
    hardware_reset();
    software_reset();

    while(!get_DREQ);

    //set SCI_MODE
    SCI_write(SCI_MODE_ADDR, 0x08, 0x40);//stream mode and sdinew enabled

    //set clock speed to 12.288MHZ
    SCI_write(SCI_CLOCKF_ADDR, 0x9B, 0xE8);//0xC3E8

    //set bass
    SCI_write(SCI_BASS_ADDR, 0x0, 0x0);

    //set volume
    SCI_write(SCI_VOL_ADDR, vol, vol);

    //0xC3E8: add=0, 36MHz
    //0x9BE8: in manual, add=3
    //0x83E8: add=0, 48MHz
    //0xA000: from a website
    //0x8BE8: Avi's settings
}

void sine_test_init() {
    uint8_t vol = 0x4F;
    set_val_vol(vol);

    disable_xdcs();
    disable_xcs();

    xCS.setAsOutput();
    xDCS.setAsOutput();
    DREQ.setAsInput();
    xRESET.setAsOutput();

    //initialize SSP0
    ssp0_init(0);

    //toggle hardware reset
    hardware_reset();

    //set volume
    SCI_write(SCI_VOL_ADDR, vol, vol);

    //set SCI_MODE
    SCI_write(SCI_MODE_ADDR, 0x08, 0x20);
}

void SCI_write(SCI_REG reg, uint8_t MSBdata, uint8_t LSBdata) {
    disable_xdcs();
    enable_xcs();
    ssp0_exchange_byte(WRITE);
    ssp0_exchange_byte(reg);
    ssp0_exchange_byte(MSBdata);
    ssp0_exchange_byte(LSBdata);
    disable_xcs();
    delay_us(100);
    while(DREQ.read() == 0);//wait until DREQ is 1 (not busy)
}

uint8_t* SDI_write(uint8_t *buffer) {
    int count = 32;

    disable_xcs();
    enable_xdcs();

    while (count) {//while count > 0
        ssp0_exchange_byte(*buffer);//de-reference pointer
        count--;
        buffer++;
    }

    disable_xdcs();

    return buffer;
}

uint16_t SCI_read(SCI_REG reg) {
    uint16_t data = 0;

    disable_xdcs();
    enable_xcs();
    ssp0_exchange_byte(READ);
    ssp0_exchange_byte(SCI_VOL_ADDR);
    data = ssp0_exchange_byte(0x00);//dummy byte
    data = data << 8;
    data |= ssp0_exchange_byte(0x00);//dummy byte
    disable_xcs();

    return data;
}

void sine_test(uint8_t frequency, uint8_t vol) {
    sine_test_init(vol);

    enable_xdcs();
    delay_us(100);
    ssp0_exchange_byte(0x53);
    ssp0_exchange_byte(0xEF);
    ssp0_exchange_byte(0x6E);
    ssp0_exchange_byte(frequency);
    ssp0_exchange_byte(0x00);
    ssp0_exchange_byte(0x00);
    ssp0_exchange_byte(0x00);
    ssp0_exchange_byte(0x00);
    disable_xdcs();
}

//void varinder_play_MP3() {
//    //load song names into array
//    char song_name[get_numb_of_tracks()][50];//4 rows of 50chars
//    strcpy(song_name[0], "1:Track1.mp3");
//    strcpy(song_name[1], "1:Track2.mp3");
//    strcpy(song_name[2], "1:Track3.mp3");
//    strcpy(song_name[3], "1:Track4.mp3");
//    //... put one strcpy per track
//
//    uint8_t buffer[2048] = {0};
//    uint8_t *mp3_ptr = buffer;
//    uint8_t *buff_ptr = buffer;
//    uint8_t vol = 0x4F;
//    init(vol);
//
//    FATFS FatFs;
//    FIL mp3_file;
//    FRESULT ret_val;
//
//    UINT buff_size = sizeof(buffer);
//    unsigned int bytes_rd = 0;
//
//    f_mount(&FatFs, "", 0);
//
//    for (int x=0; x<get_numb_of_tracks(); x++) {
//        ret_val = f_open(&mp3_file, song_name[x], FA_READ);
//
//        while (!f_eof(&mp3_file)) {
//            f_read(&mp3_file, mp3_ptr, buff_size, &bytes_rd);
//            for(unsigned int i=0; i<sizeof(buffer); i=i+32) {
//
//                //MUST DO! When its low wait for a rising edge interrupt and if its low
//                //Check data request pin is low, loop until its high
//                while(!get_DREQ());
//
//                buff_ptr = SDI_write(buff_ptr);
//
//            }
//            buff_ptr = buffer;
//        }
//
//        f_close(&mp3_file);
//    }
//}
