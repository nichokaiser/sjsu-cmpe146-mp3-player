#ifndef MP3_TASKS_HPP_
#define MP3_TASKS_HPP_

#include <stdio.h>
#include <stdint.h>
#include "utilities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "uart0_min.h"
#include "printf_lib.h"
#include "LPC17xx.h"
#include "semphr.h"
#include "lpc_isr.h"
#include "io.hpp"
#include <time.h>
#include <storage.hpp>
#include "event_groups.h"
#include "command_handler.hpp"
#include "lpc_sys.h"
#include "tasks.hpp"
#include "ssp1.h"
#include "ssp0.h"
#include "gpio.hpp"
#include "decode.hpp"
#include "UART_LCD.hpp"

typedef struct {
    char song_name[100];
    char directory_name[100];
} mp3_t;

FRESULT numb_of_files(char* path);
mp3_t* get_file_info();

uint8_t get_val_vol();
uint8_t get_val_vsbe();
uint8_t get_val_vstc();
int get_numb_of_tracks();

void set_val_vol(uint8_t vol);
void set_val_eq(uint8_t treb, uint8_t bass);
void set_numb_of_tracks(int val);

void incr_vol();
void decr_vol();

void volEncoder(void *p);
void eqEncoder(void *p);
void playSound(void *p);
void suspend(void *p);
void lcdDisplay(void *p);


int next_song();
int prev_song();
void play_pause(TaskHandle_t gpioPanelHandle, TaskHandle_t eqEncoderHandle, TaskHandle_t volEncoderHandle, TaskHandle_t playSoundHandle, TaskHandle_t suspendHandle);
int stop_reset(TaskHandle_t gpioPanelHandle, TaskHandle_t eqEncoderHandle, TaskHandle_t volEncoderHandle, TaskHandle_t playSoundHandle, TaskHandle_t suspendHandle);

#endif /* MP3_TASKS_HPP_ */
