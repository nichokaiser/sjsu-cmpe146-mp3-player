
#include <stdio.h>
#include <stdint.h>
#include "utilities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "LPC17xx.h"
#include "semphr.h"
#include "lpc_isr.h"
#include "lpc_sys.h"
#include "tasks.hpp"
#include "ssp0.h"
#include "gpio.hpp"
#include "decode.hpp"
#include <string.h>
#include "MP3_Tasks.hpp"
#include "UART_LCD.hpp"

//play, pause, next, prev, stop
//fill in tasks in MP3_Tasks driver
SemaphoreHandle_t semaphoreHandle1 = NULL;

TaskHandle_t gpioPanelHandle = NULL;
TaskHandle_t eqEncoderHandle = NULL;
TaskHandle_t volEncoderHandle = NULL;
TaskHandle_t playSoundHandle = NULL;
TaskHandle_t suspendHandle = NULL;

void gpioISR(void){
    long yield = 0;
    LPC_GPIOINT->IO2IntClr |= (1 << 0);
    LPC_GPIOINT->IO2IntClr |= (1 << 1);
    LPC_GPIOINT->IO2IntClr |= (1 << 2);
    LPC_GPIOINT->IO2IntClr |= (1 << 3);
    xSemaphoreGiveFromISR(semaphoreHandle1, &yield);
    portYIELD_FROM_ISR(yield);

}

void gpioInit(void){
    LPC_GPIO2->FIODIR &= ~(1 << 0);
    LPC_PINCON->PINMODE4 |= (3 << 0);
    LPC_PINCON->PINSEL4 &= ~(3 << 0);
    LPC_GPIOINT->IO2IntEnR |= (1 << 0);

    LPC_GPIO2->FIODIR &= ~(1 << 1);
    LPC_PINCON->PINMODE4 |= (3 << 2);
    LPC_PINCON->PINSEL4 &= ~(3 << 2);
    LPC_GPIOINT->IO2IntEnR |= (1 << 1);

    LPC_GPIO2->FIODIR &= ~(1 << 2);
    LPC_PINCON->PINMODE4 |= (3 << 4);
    LPC_PINCON->PINSEL4 &= ~(3 << 4);
    LPC_GPIOINT->IO2IntEnR |= (1 << 2);

    LPC_GPIO2->FIODIR &= ~(1 << 3);
    LPC_PINCON->PINMODE4 |= (3 << 6);
    LPC_PINCON->PINSEL4 &= ~(3 << 6);
    LPC_GPIOINT->IO2IntEnR |= (1 << 3);

    isr_register(EINT3_IRQn, gpioISR);
    NVIC_EnableIRQ(EINT3_IRQn);
}

void gpioPanel(void *p){
    gpioInit();
    while(1){
        if(xSemaphoreTake(semaphoreHandle1, portMAX_DELAY)){
            if(LPC_GPIO2->FIOPIN & (1 << 0)){//P2.0
                prev_song();
            }
            if(LPC_GPIO2->FIOPIN & (1 << 1)){//P2.1
                stop_reset(gpioPanelHandle, eqEncoderHandle, volEncoderHandle, playSoundHandle, suspendHandle);
            }
            if(LPC_GPIO2->FIOPIN & (1 << 2)){//P2.2
                play_pause(gpioPanelHandle, eqEncoderHandle, volEncoderHandle, playSoundHandle, suspendHandle);
            }
            if(LPC_GPIO2->FIOPIN & (1 << 3)){//P2.3
                next_song();
            }
            vTaskDelay(200); //(250); //debounce
        }
    }
}

int main() {
    const uint32_t STACK_SIZE = 1024;
    semaphoreHandle1 = xSemaphoreCreateBinary();

    //LCD_test();

    mp3_t* ptr = get_file_info();

     xTaskCreate(
        gpioPanel,
        "gpioPanels",
        STACK_SIZE,
        NULL,
        1, //HIGH Priority
        &gpioPanelHandle
    );

     xTaskCreate(
        eqEncoder,
        "EQ",
        STACK_SIZE,
        NULL,
        1, //LOW Priority
        &eqEncoderHandle
    );

     xTaskCreate(
        volEncoder,
        "VOL",
        STACK_SIZE,
        NULL,
        1, //LOW Priority
        &volEncoderHandle
    );

    xTaskCreate(
        lcdDisplay,
        "LCD",
        STACK_SIZE,
        (void*) ptr,
        1, //HIGH Priority
        NULL
    );

     xTaskCreate(
        playSound,
        "MP3",
        STACK_SIZE,
        (void*)ptr, //void * const pvParameters
        1, //HIGH Priority
        &playSoundHandle
    );

      xTaskCreate(
        suspend,
        "suspend",
        STACK_SIZE,
        (void*)playSoundHandle, //void * const pvParameters
        1, //HIGH Priority
        &suspendHandle
    );


     scheduler_add_task(new terminalTask(PRIORITY_HIGH));
     scheduler_start();

    vTaskStartScheduler();

    return 0;
}
