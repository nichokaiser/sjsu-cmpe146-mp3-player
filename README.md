# SJSU CMPE146 MP3 Player

This is the MP3 Player project code for the course CMPE146 at San Jose State University in Spring 2018.

## Authors and acknowledgment
This code has been posted several years after the completion of the project and was written by all of the following project members:
- David Bit-Heydari
- Nicholas Frangos
- Nicholas Kaiser
- Varinder Singh
